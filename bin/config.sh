##### SET ACCORDING TO YOUR SYSTEM CONFIGURATION ########
#                                                       #
# This file should contain all paths to dependent tools #
# and other required files.                             #
#                                                       #
#########################################################


############### DEPENDENCIES LOCATION ######################

# download location of the ChIP-eat pipeline
CHIP_EAT_FOLDER="/storage/mathelierarea/processed/rafael/chip-eat/"
DATA_FOLDER="/storage/mathelierarea/processed/rafael/data/"

PATH_TO_BOWTIE2="/lsc/bowtie2"
PATH_TO_SAMTOOLS="/lsc/samtools/bin"
PATH_TO_BEDTOOLS2="/lsc/bedtools2/bin/"

# absolute paths to the peak caller executable
# e.g., /home/user/HOMER/bin
PATH_TO_MACS="/lsc/macs2/bin/macs2"
## optional peak callers
PATH_TO_BCP="/storage/mathelierarea/processed/rafael/chip-eat/src/BCP_v1.1"
PATH_TO_HOMER="/lsc/homer/bin"
PATH_TO_GEM="/storage/mathelierarea/processed/marius/PEAK/src/gem"

# maximum Java heap memory size for the GEM peak caller
JAVA_MEM=16G

# genome related for the mapping and coversion to FASTA

if [ "$species" == "Hsapiens" ]
then
	GENOME_FILE="/storage/mathelierarea/raw/UCSC/hg38/Sequence/WholeGenomeFasta/genome.fa"
	CHROMOSOME_FILE="/storage/mathelierarea/raw/UCSC/hg38/Sequence/WholeGenomeFasta/hg38_chrom_sizes"
	PATH_TO_GB_SHAPE="/storage/mathelierarea/raw/GBshape/hg38"
	BACKGROUND_MAPPABLE_GENOME=$DATA_FOLDER"/mappable_genome/hg38"
	BACKGROUND_REPOSITORY=$DATA_FOLDER"/BiasAway_repository/hg38"
elif [ "$species" == "Mmusculus" ]
then
	GENOME_FILE="/storage/mathelierarea/raw/UCSC/mm10/Sequence/WholeGenomeFasta/mm10.fa"
	CHROMOSOME_FILE="/storage/mathelierarea/raw/UCSC/mm10/Sequence/WholeGenomeFasta/mm10.chrom.sizes"
	PATH_TO_GB_SHAPE="/storage/mathelierarea/raw/GBshape/mm10"
	BACKGROUND_MAPPABLE_GENOME=$DATA_FOLDER"/mappable_genome/mm10"
	BACKGROUND_REPOSITORY=$DATA_FOLDER"/BiasAway_repository/mm10"
elif [ "$species" == "Rnorvegicus" ]
then
	GENOME_FILE="/storage/mathelierarea/raw/UCSC/Rnor_6.0/Sequence/WholeGenomeFasta/Rnor_6.0.fa"
        CHROMOSOME_FILE="/storage/mathelierarea/raw/UCSC/Rnor_6.0/Sequence/WholeGenomeFasta/Rnor_6.0.chrom.sizes"
        PATH_TO_GB_SHAPE="/storage/mathelierarea/raw/GBshape/Rnor_6.0"
	BACKGROUND_MAPPABLE_GENOME=$DATA_FOLDER"/mappable_genome/Rnor_6.0"
	BACKGROUND_REPOSITORY=$DATA_FOLDER"/BiasAway_repository/Rnor_6.0"
elif [ "$species" == "Drerio" ]
then
	GENOME_FILE="/storage/mathelierarea/raw/UCSC/GRCz11/Sequence/WholeGenomeFasta/GRCz11.fa"
	CHROMOSOME_FILE="/storage/mathelierarea/raw/UCSC/GRCz11/Sequence/WholeGenomeFasta/GRCz11.chrom.sizes"
	PATH_TO_GB_SHAPE="/storage/mathelierarea/raw/GBshape/GRCz11"
	BACKGROUND_MAPPABLE_GENOME=$DATA_FOLDER"/mappable_genome/GRCz11"
	BACKGROUND_REPOSITORY=$DATA_FOLDER"/BiasAway_repository/GRCz11"
elif [ "$species" == "Celegans" ]
then
	GENOME_FILE="/storage/mathelierarea/raw/UCSC/WBcel235/Sequence/WholeGenomeFasta/WBcel235.fa"
	CHROMOSOME_FILE="/storage/mathelierarea/raw/UCSC/WBcel235/Sequence/WholeGenomeFasta/WBcel235.chrom.sizes"
	PATH_TO_GB_SHAPE="/storage/mathelierarea/raw/GBshape/WBcel235"
	BACKGROUND_MAPPABLE_GENOME=$DATA_FOLDER"/mappable_genome/WBcel235"
	BACKGROUND_REPOSITORY=$DATA_FOLDER"/BiasAway_repository/WBcel235"
elif [ "$species" == "Dmelanogaster" ]
then
	GENOME_FILE="/storage/mathelierarea/raw/UCSC/dm6/Sequence/WholeGenomeFasta/dm6.fa"
	CHROMOSOME_FILE="/storage/mathelierarea/raw/UCSC/dm6/Sequence/WholeGenomeFasta/dm6.chrom.sizes"
	PATH_TO_GB_SHAPE="/storage/mathelierarea/raw/GBshape/dm6"
	BACKGROUND_MAPPABLE_GENOME=$DATA_FOLDER"/mappable_genome/dm6"
	BACKGROUND_REPOSITORY=$DATA_FOLDER"/BiasAway_repository/dm6"
elif [ "$species" == "Scerevisiae" ]
then
	GENOME_FILE="/storage/mathelierarea/raw/UCSC/R64-1-1/Sequence/WholeGenomeFasta/R64-1-1.fa"
	CHROMOSOME_FILE="/storage/mathelierarea/raw/UCSC/R64-1-1/Sequence/Chromosomes/chrom.sizes"
	PATH_TO_GB_SHAPE="/storage/mathelierarea/raw/GBshape/R64-1-1"
	BACKGROUND_REPOSITORY=$DATA_FOLDER"/BiasAway_repository/R64-1-1"
	BACKGROUND_MAPPABLE_GENOME=$DATA_FOLDER"/mappable_genome/R64-1-1"
elif [ "$species" == "Spombe" ]
then
        GENOME_FILE="/storage/mathelierarea/raw/UCSC/ASM294v2/Sequence/WholeGenomeFasta/ASM294v2.fa"
        CHROMOSOME_FILE="/storage/mathelierarea/raw/UCSC/ASM294v2/Sequence/Chromosomes/chrom.sizes"
        PATH_TO_GB_SHAPE="/storage/mathelierarea/raw/GBshape/ASM294v2"
        BACKGROUND_REPOSITORY=$DATA_FOLDER"/BiasAway_repository/ASM294v2"
        BACKGROUND_MAPPABLE_GENOME=$DATA_FOLDER"/mappable_genome/ASM294v2"
elif [ "$species" == "Athaliana" ]
then
	GENOME_FILE="/storage/mathelierarea/raw/UCSC/TAIR10/Sequence/WholeGenomeFasta/TAIR10.fa"
	CHROMOSOME_FILE="/storage/mathelierarea/raw/UCSC/TAIR10/Sequence/WholeGenomeFasta/TAIR10.chrom.sizes"
	PATH_TO_GB_SHAPE="/storage/mathelierarea/raw/GBshape/TAIR10"
	BACKGROUND_MAPPABLE_GENOME=$DATA_FOLDER"/mappable_genome/TAIR10"
	BACKGROUND_REPOSITORY=$DATA_FOLDER"/BiasAway_repository/TAIR10"
fi

PATH_TO_BOWTIE_INDEX="/lsc/bowtie2ind/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.bowtie_index"


# GB shape files - the DNA feature files (modify according to your file naming)
# HELT - Helix Twist 
# MGW - Minor Groove Width
# PROT - Propeller Twist
# ROLL - The Roll :|

#first order NEED TO CHANGE!!!!
HELT=$PATH_TO_GB_SHAPE"/hg38.HelT.wig.bw"
MGW=$PATH_TO_GB_SHAPE"/hg38.MGW.wig.bw"
PROT=$PATH_TO_GB_SHAPE"/hg38.ProT.wig.bw"
ROLL=$PATH_TO_GB_SHAPE"/hg38.Roll.wig.bw"
#second order NEED TO CHANGE!!!!
HELT2=$PATH_TO_GB_SHAPE"/hg38.HelT.2nd.wig.bw"
MGW2=$PATH_TO_GB_SHAPE"/hg38.MGW.2nd.wig.bw"
PROT2=$PATH_TO_GB_SHAPE"/hg38.ProT.2nd.wig.bw"
ROLL2=$PATH_TO_GB_SHAPE"/hg38.Roll.2nd.wig.bw"


############### INTERNAL TO THE PIPELINE ##############
# IMPORTANT: MODIFY ONLY IF YOU CHANGE THE DEFAULT LOCATION OF THE FILES
ALIGNMENT_SCRIPT=$CHIP_EAT_FOLDER"/bin/alignment/alignment.sh"
PEAK_CALLING_SCRIPT=$CHIP_EAT_FOLDER"/bin/peak_calling/peak_calling.sh"
PEAK_PROCESSING_SCRIPT=$CHIP_EAT_FOLDER"/bin/peak_processing/peak_processing.sh"
PEAK_PROCESSING_SCRIPT_GEO=$CHIP_EAT_FOLDER"/bin/peak_processing/peak_processing_GEO.sh"

# needed by the peak processing scripts
PWM_SCORING_CODE=$CHIP_EAT_FOLDER"/bin/utils/pwm_searchPFF"
BEM_SCORING_CODE=$CHIP_EAT_FOLDER"/bin/utils/energy_searchPFF.py"
TFFM_MODULE=$CHIP_EAT_FOLDER"/src/TFFM-master"
TFFM_SCORING_CODE_FO=$CHIP_EAT_FOLDER"/bin/utils/TFFM_first_order.py"
TFFM_SCORING_CODE_FO_AND_DETAILED=$CHIP_EAT_FOLDER"/bin/utils/TFFM_first_order_and_detailed.py"
FORMAT_PFM_FOR_DAMO_CODE=$CHIP_EAT_FOLDER"/bin/utils/format_PFM_for_DAMO.R"
DAMO_TRAIN_CODE=$CHIP_EAT_FOLDER"/bin/utils/applyDAMO.sh"
DAMO_CODE=$CHIP_EAT_FOLDER"/src/DAMO/DAMO.py"
DIMO_TRAIN_SCODE=$CHIP_EAT_FOLDER"/bin/utils/train_DiMO.R"
DIMO_APPLY_SCODE=$CHIP_EAT_FOLDER"/bin/utils/call_DiMO.R"
DNA_SHAPED_CODE=$CHIP_EAT_FOLDER"/src/DNAshapedTFBS-master/DNAshapedTFBS.py"
JOIN_RESCANNED_PEAKS=$CHIP_EAT_FOLDER"/bin/utils/get_rescanned_within_thr.R"
REVERSE_COMPLEMENT_CODE=$CHIP_EAT_FOLDER"/bin/utils/reverse_complement_negative_strands.R"
GET_BEST_TFBS=$CHIP_EAT_FOLDER"/bin/utils/get_best_TFBS_in_EZ_per_Peak.R"
RECOVER_MACS=$CHIP_EAT_FOLDER"/bin/utils/recover_peaks_with_tfbs.R"

# needed for TFFM, DiMO and DNAshaped. Generates background files matching the GC composition
# go through the READ.ME here https://github.com/wassermanlab/BiasAway to understand
GENERATE_BACKGROUND_SCRIPT=$CHIP_EAT_FOLDER"/src/BiasAway-noRPY/BiasAway.py"
BACKGROUND_UTILS=$CHIP_EAT_FOLDER"/bin/utils"
BACKGROUND_DUMMY_FILE=$BACKGROUND_UTILS"/dummy_file_bias_away.fa" #used in the repository generation
BACKGROUND_CALL_SEQUENCE_FILTERING=$BACKGROUND_UTILS"/call_filter_sequences.R"
BACKGROUND_SEQUENCE_FILTERING=$BACKGROUND_UTILS"/filter_sequences.R"
GENERATE_BACKGROUND_REPOSITORY=$BACKGROUND_UTILS"/create_background_repository.sh"
GENERATE_BACKGROUND_FILES=$BACKGROUND_UTILS"/generate_background_files.sh"

# defining the enrichment zone
COMPUTE_THRESHOLDS_SCRIPT=$CHIP_EAT_FOLDER"/bin/utils/compute_thresholds.R"
CALL_THRESHOLDS_SCRIPT=$CHIP_EAT_FOLDER"/bin/utils/call_thresholds.R"
ENTROPY_CODE=$CHIP_EAT_FOLDER"/bin/utils"

# the file containing the mapping between input ChIP-seq datasets, their TF and the PFM to use
#TF_MAPPING=$CHIP_EAT_FOLDER"/bin/utils/TF_mapping_merged.tsv"
TF_MAPPING="/storage/mathelierarea/processed/rafael/TF_mapping_spec/TF_mapping.tsv"

#location of the folders containing the PFMs in different formats - used in the mapping process
PWM_FOLDER=$CHIP_EAT_FOLDER"/data/PWM_JASPAR"
MEME_FOLDER=$CHIP_EAT_FOLDER"/data/MEME_JASPAR"
PFM_FOLDER=$CHIP_EAT_FOLDER"/data/PFM_JASPAR2020/Processed"


