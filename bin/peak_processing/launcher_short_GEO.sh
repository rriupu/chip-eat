#!/bin/bash

results_dir=$1;
database=$2;
species=$3;
processors=$4;

if [ $species = "Hsapiens" ]
  then
    species_db="Homo_sapiens"
elif [ $species = "Mmusculus" ]
  then
    species_db="Mus_musculus"
elif [ $species = "Rnorvegicus" ]
  then
    species_db="Rattus_norvegicus"
elif [ $species = "Drerio" ]
  then
    species_db="Danio_rerio"
elif [ $species = "Dmelanogaster" ]
  then
    species_db="Drosophila_melanogaster"
elif [ $species = "Celegans" ]
  then
    species_db="Caenorhabditis_elegans"
elif [ $species = "Athaliana" ]
  then
    species_db="Arabidopsis_thaliana"
elif [ $species = "Scerevisiae" ]
  then
    species_db="Saccharomyces_cerevisiae"
elif [ $species = "Spombe" ]
  then
    species_db="Schizosaccharomyces_pombe"
fi

if [ $database = "GEO" ]
  then
    in_dir="/storage/mathelierarea/processed/rafael/data/Peaks/GEO/To_process/"
elif [ $database = "ENCODE" ]
  then
    in_dir="/storage/mathelierarea/processed/rafael/data/Peaks/ENCODE/MACS/"
elif [ $database = "GTRD" ]
  then
    in_dir="/storage/mathelierarea/processed/rafael/data/autoupdate/GTRD/New_datasets_to_process/Organisms/"$species_db"/"
fi

find $results_dir -maxdepth 1 -mindepth 1 -type d | xargs -I {} basename {} | xargs -I {} --max-proc=$processors bash /storage/mathelierarea/processed/rafael/chip-eat/bin/peak_processing/peak_processing_short_GEO.sh ${in_dir}/{} ${results_dir}/{}

#find $results_dir -maxdepth 1 -mindepth 1 -type d | xargs -I {} --max-proc=$processors echo bash /storage/mathelierarea/processed/rafael/chip-eat/bin/peak_processing/peak_processing_short.sh ${in_dir}/{} ${results_dir}{}
