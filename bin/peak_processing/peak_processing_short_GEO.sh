#!/bin/bash

in_dir=$1;
out_dir=$2;
window_size=500;

clean_up () {
files=("$@")
for file in "${files[@]}"
  do
    if [ -f $file ]
      then
         rm $file
    fi
done
}

source "../config.sh"

#out_dir=$result_folder/$(basename $in_dir)

in=$(basename $in_dir)
folder=(${in//_/ })
tf_name=${folder[1]%/*}
tf_name="$(echo "$tf_name" | tr '[:lower:]' '[:upper:]')"

damo_dir=${out_dir}'/DAMO/'

if [ -d $damo_dir ]
then
  damo_pwms=$(find ${damo_dir} -name "*damo.pwm")
  foreground_test_file_fa=${out_dir}'/training_files/MACS_'${tf_name}'_peaks_500bp.fa'

  for new_pwm in ${damo_pwms[@]}
    do
      $PWM_SCORING_CODE $new_pwm $foreground_test_file_fa 0.0 -b -n $tf_name > $out_dir'/DAMO/MACS_'${tf_name}_${new_pwm##*/}_norescan
      $PWM_SCORING_CODE $new_pwm $foreground_test_file_fa 0.0 -n $tf_name > $out_dir'/DAMO/MACS_'${tf_name}_${new_pwm##*/}_rescan
      out_file=$out_dir'/DAMO/MACS_'${tf_name}_${new_pwm##*/}_norescan
      out_file_rescan=$out_dir'/DAMO/MACS_'${tf_name}_${new_pwm##*/}_rescan

      ################# 5. Format file for thresholding ####################
      Rscript $REVERSE_COMPLEMENT_CODE $out_file

      awk -v OFS='\t' -F"::" '$1=$1' $out_file | \
      awk -v OFS='\t' '{print $2,$3,$4,$5,$6,$7,$8,$9,$10,$1}' | \
      awk -v OFS='\t' -F":" '$1=$1' | \
      awk -v OFS='\t' '{gsub(/\-/, "\t", $2)} 1' | \
      awk -v OFS='\t' '{print $1,$2,$3,$6,$11,$5,(($10-$9)+1),(($9+$10)/2)-'$window_size',$8,$7,$9,$10,$12}' > ${out_file}.score

      Rscript $REVERSE_COMPLEMENT_CODE $out_file_rescan

      awk -v OFS='\t' -F"::" '$1=$1' $out_file_rescan | \
      awk -v OFS='\t' '{print $2,$3,$4,$5,$6,$7,$8,$9,$10,$1}' | \
      awk -v OFS='\t' -F":" '$1=$1' | \
      awk -v OFS='\t' '{gsub(/\-/, "\t", $2)} 1' | \
      awk -v OFS='\t' '{print $1,$2,$3,$6,$11,$5,(($10-$9)+1),(($9+$10)/2)-'$window_size',$8,$7,$9,$10,$12}' > ${out_file_rescan}.score

      to_remove+=($out_file)
      to_remove+=($out_file_rescan)
      to_remove+=(${out_file_rescan}.score)
                   
      thresholds=$(Rscript $CALL_THRESHOLDS_SCRIPT $COMPUTE_THRESHOLDS_SCRIPT $CHIP_EAT_FOLDER $ENTROPY_CODE ${out_file}.score "relative" 7 8 9 $window_size ${out_file}.ez)
                   
      if [ "$thresholds" !=  "OK" ]
        then
          continue
      fi

      #################### 7. Create BED6 file with the non-rescanned TFBSs #################
      if [ -f ${out_file}.ez ] && [ -s ${out_file}.ez ]
        then
          awk -v OFS='\t' '{print $1, ($2+$11), ($2+$12), $6 "_" $5, ".", $4}' ${out_file}.ez > ${out_file}.tfbs.bed
      fi

      ################### 8. Subset rescanned sequences within the enrichment zone and create final output file ###################
   
      peaks_file=(`find $in_dir -name "*_peaks.narrowPeak"`)
      Rscript /storage/mathelierarea/processed/rafael/chip-eat/bin/utils/get_rescanned_within_thr.R ${out_file}.score.thr ${out_file}.score ${out_file_rescan}.score $peaks_file
       
      Rscript $GET_BEST_TFBS ${out_file_rescan}.score.ez.score

      ################### 9. Recover original ChIP-seq peaks containing TFBSs ####################

      peaks_file=(`find $in_dir -name "*_peaks.narrowPeak"`)
      Rscript $RECOVER_MACS $peaks_file ${out_file_rescan}.score.ez.score_best.tsv ${out_file_rescan}'_peaks_with_tfbs_ez.bed'
    
  done #end for over PFMs

  clean_up "${to_remove[@]}"
fi
