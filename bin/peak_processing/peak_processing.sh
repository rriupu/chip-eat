#!/bin/bash

# This script will execute the peak processing part of the pipeline. It will process one set of 
# peaks with either one of the four different prediction models avalable, or with all of them.
# Those models are: PWM (also DiMO), binding energy model (BEM), transcription factor flexible
# models (TFFM) and DNA-shaped TFBSs (DNA). To call all models use ALL. It can receive as input
# a set of peaks from one of the supported peak callers (MACS2, BCP, HOMER, GEM) or any set of 
# peaks in narrowPeak format. For the latter casem you can pass as data type "MACS" and it will 
# be able to process it.
# 
# The set of peaks are processed to identify the peak summit, then the area around the peak summit is 
# increased with 500bp (default) upstream and 500bp (default) downstream from the peak summit, and 
# the FASTA sequence for the corresponding regions is retrieved. After the scoring phase of each model,
# an enrichment zone for the high confidence TFBS predictions is automatically set based on a 
# non-parametric, data driven entropy-based algorithm. The TFBSs predicted within this region are
# exported under a BED6 format. Please refer to the READ.ME file for more details about the output. 
# 
# 
# Dependencies - external: 
# -------------
# bedtools2 
# java 1.8
# R >= 2.5.0
# R packages: rJava 0.9-9, MASS (should be installed by default)
# python 2.7
# python libraries: pandas, numpy, Bio, ArgumentParser, joblib, xgboost, scikit-learn
#
# Prediction models (also provided in the src folder):
# ---------------------------------------------------
# DiMO: http://stormo.wustl.edu/DiMO/
# TFFM: https://github.com/wassermanlab/TFFM
# DNAshaped: https://github.com/amathelier/DNAshapedTFBS
# BEM: in the bin/utils
# Background generator matching GC%:
# ---------------------------------
# BiasAway: https://github.com/wassermanlab/BiasAway
#
# Dependencies - internal
# -----------------------
# PFMs - a complete collection of PFMs, PWMs and PFMs in MEME format from JASPAR 2018
# are provided in the 'data' folder of the pipeline. They are formatted to work with 
# all the prediction models used in this pipeline. You can also provide a PFM of your own
# Check the parameters. 
#
# A mapping file between TFs and the corresponding JASPAR profile(s) is provided in 'bin/utils'
# NOTE: that not all TFs have a JASPAR profile.
#
# Most of the files that are present in the 'bin/utils' will be used by this script.
# Do not remove them or change them unless you trace your changes down to the 'bin/config.sh' file
#
# usage bash peak_calling.sh /path/to/input/data/set /path/to/output/folder MACS
# usage="bash launch_peak_processing.sh /my/input/folder /my/output/folder/ MACS DiMO 20 [my_pfm] [my_tf_name] [window_size] [training_window_size] "

# load the paths of all dependencies 
# NOTE: it assumes that the config.sh file is in the bin folder of the pipeline
#source "../config.sh"

usage="usage: bash peak_processing.sh /my/input/folder /my/output/folder/ MACS DiMO Hsapiens [my_pfm] [my_tf_name] [window_size] [training_window_size] "


################### ARGUMENT PARSING AND PARAMETER SETTING #########################
in_dir=
if [ -z "$1" ]
    then
        echo "No input file specified. Exiting...";
        echo $usage
        exit 1
    else
        in_dir=$1
fi

result_folder=;
if [ -z "$2" ]
    then
        echo "No result folder specified. Exiting...";
        echo $usage
        exit 1
    else
        result_folder=$2
fi

# the peak caller used (just for the folder/file naming 
data_type=;
if [ -z "$3" ]
    then
        echo "No data type specified. Exiting...";
        echo $usage
        exit 1
    else
        data_type=$3
fi

model=;
if [ -z "$4" ]
    then
        echo "No prediction model specified. Use ALL or one of: PWM, DiMO, BEM, TFFM or DNA. Exiting...";
        echo $usage
        exit 1
    else
        model=$4
fi

species=;
if [ -z "$5" ]
    then
        echo "No species specified. Options are: Hsapiens, Mmusculus, Rnorvegicus, Drerio, Celegans, Dmelanogaster, Scerevisiae, Spombe and Athaliana. Exiting..."
        echo $usage
        exit 1
    else
        species=$5
fi

# optional arguments
pfm=;
if [ ! -z "$6" ]
    then
        pfm=$6
        pfms=($pfm)
fi

tf_name=;
if [ ! -z "$7" ]
    then
        tf_name=$7
fi

window_size=;
if [ ! -z "$8" ]
    then
        window_size=$8
    else
        # just to make sure
        window_size=500
fi

training_window_size=;
if [ ! -z "$9" ]
    then
        training_window_size=$9
    else
        # just to make sure
        training_window_size=50
fi

verbose=;
if [ ! -z "$10" ]
    then
       # can be whatever
       verbose=$10
fi

source "../config.sh"

# Function to write to log. Note that if "verbose" is set,
# the log messages will be on the std also
logme () {
    if [ ! -z $verbose ]
       then 
           echo "${*}" >> $log 2>&1;
    else
           echo "${*}" >> $log
    fi
}

#Function to remove intermediate files produced during processing
clean_up () {
files=("$@")
for file in "${files[@]}"
  do
    if [ -f $file ]
      then
         rm $file
    fi
done
}

# Retrieve the right PFM(s) identifier(s) for this TF based on mapping file and model
# Columns of the mapping file should follow this order:
# 1. TF_NAME: official name of the transcription factor
# 2. PFM_ID: the JASPAR identifier for the PFM corresponding to this TF
# 3. VERSION: the version of the PFM for this TF (should always be the last/newest one used)
# 4. DATA_SET: the name of the ChIP-seq data set
retrieve_pfms () {

     noPFM=true
     pfm_id=;
     pfm_version=;
     pfm_folder="$1"
 
     while IFS=$'\t' read -r -a line
         do
           data_set_map="${line[3]}"
           data_set_map="$(echo "$data_set_map" | tr '[:lower:]' '[:upper:]')"

           # speed up the file parsing a bit
           if [[ "$data_set_map" < "$data_set" ]]
              then
                  continue
           elif [[ "$data_set_map" == "$data_set" ]]
              then
                  data_set_tf_name="${line[0]}"
                  pfm_id="${line[1]}"
                  pfm_version="${line[2]}"

                  #to upper case
                  data_set_tf_name="$(echo "$data_set_tf_name" | tr '[:lower:]' '[:upper:]')"
           elif [[ "$data_set_map" > "$data_set" ]]
             then
                  break
           fi

           # check if we got a match on data set, cell line and TF
           if [ "$data_set" == "$data_set_map" ] && [ "$tf_name" == "$data_set_tf_name" ]
             then
                 #is there a PFM for this TF
                 if [ "$pfm_id" == "NA" ]
                    then
                    logme "No PFM for the transcription factor $data_set_tf_name in data set $data_set was found. Skipping.."
                    #rm -R $out_dir
		    exit 0
                    # remove parent directory also if empty
                    if [ -z "$(ls -A "$(dirname $out_dir)")" ]
                        then
                           rm -R "$(dirname $out_dir)"
                    fi
                        break
                else
                    pfms+=($pfm_folder/${pfm_id}.${pfm_version})
                    noPFM=false
                fi
           else
                logme "Something does not match. Make sure the mapping file is correct!"
                logme "Dataset from file name: $data_set" 
                logme "Dataset from map: $data_set_map" 
                logme "TF from file name: $tf_name"
                logme "TF from map: $data_set_tf_name" 
                logme "---------------------------------------"
           fi
 done < $tf_pfm_map

 # no mapping for this dataset - log and skip the folder
 if [[ -z "${pfm_id// }" ]]
      then
         logme "The data set $data_set has no entry in the TF to PWM mapping table. Skipping... "
         exit 1
 fi

  # no PFM(s) found - skip to next folder
 if $noPFM
     then
        exit 0
 fi

#return 
echo "${pfms[@]}"

}

########## Getting ready to process #########

#no PFM passed as argument, check if the TF mapping file exists
if [ -z $pfm ]
   then 
      if [ ! -e $TF_MAPPING ]
         then
             echo "No TF to PFM mapping file found. Exiting.."
             exit 1
      # and sort if necessary - change the dataset column if not 4th
      elif [ ! -e ${TF_MAPPING}.sorted ]
         then
            sort -t $'\t' -k 4 $TF_MAPPING > ${TF_MAPPING}.sorted
      fi
fi
tf_pfm_map=${TF_MAPPING}.sorted

#get the name of dataset and TF name if needed
in_dir=${in_dir%/}
in=${in_dir##*/}
folder=(${in//_/ })
data_set=${folder[0]%/*}
data_set="$(echo "$data_set" | tr '[:lower:]' '[:upper:]')"

if [ -z $tf_name ]
  then
      tf_name=${folder[-1]%/*}
      tf_name="$(echo "$tf_name" | tr '[:lower:]' '[:upper:]')"
fi

# DEBUG
#echo "TF name: $tf_name"
#echo "Dataset: $data_set"

#cd $in_dir
echo "Processing folder: $(basename $in_dir)"

   ####################### Retrieve the peaks file according to the data type #########################

   # put together the names of the output file
   out_dir=$result_folder/${in_dir##*/}
   out_file=$out_dir/${data_type}_${tf_name}_peaks

# Check if the dataset has been processed or not

   #echo "${out_dir}/DAMO/"
   to_check=${out_dir}/DAMO/
   #echo $to_check

        if [ ! -d $to_check ] && [ -d $out_dir ]
        then
                rm -r $out_dir
                processed=;
	elif [ ! -d $out_dir ]
	then
		processed=;
        else
                processed=(`find $to_check -name "*best*"`)
                #echo $processed
        fi

        if [ ! -z "$processed" ]
        then
                echo "Dataset already processed"
                exit 0
        else
                if [ -d $out_dir ]
                then
                        rm -r $out_dir
                fi
        fi

   mkdir -p $out_dir

   # DEBUG
#   echo "Out dir: $out_dir"
#   echo "Out file: $out_file"
   
   	 if [ $data_type = "MACS" ]
	     then
	        #echo "I'm in MACS";
#	        peaks_file=${in_dir%/*}/${in_dir##*/}/${in_dir##*/}_peaks.narrowPeak
	        peaks_file=(`find $in_dir -name "*_peaks.narrowPeak"`)
		if [ -e $peaks_file ]
	           then
        	      # add offset to chrStart 
	              awk -v OFS='\t' '{print $1,$2+$10,$2+$10+1,$4,$5,$6}' $peaks_file > $out_file
        	   else
	              echo "Folder $in_dir does not contain a results file. Check the MACS log file for more details. Skipping..";
        	      exit 1
        fi
	  elif [ $data_type = "HOMER" ]
	     then
	       # echo "I'm in HOMER.. doh";
	        peaks_file=${in_dir%/*}/${in_dir##*/}/${in_dir##*/}_peaks.bed
	        if [ -e $peaks_file ]
	           then

	       	      # remove the header of the file with the parameter settings
        	      sed '/^#/ d' $peaks_file > ${peaks_file}_no_header

	              # calculate peak summit as the middle of the distance between start and end  
	              awk -v OFS='\t' '{print $2,$3+int(($4-$3)/2), $3+int(($4-$3)/2+1),"'${in_dir##*/}'",$9,$5}' ${peaks_file}_no_header > $out_file
	           else
	              echo "Folder $in_dir does not contain a results file. Check the HOMER BED file for more details. Skipping..";
        	      exit 1
	        fi
	  elif [ $data_type = "BCP" ]
	     then
	       # echo "I'm in BCP";
	        peaks_file=${in_dir%/*}/${in_dir##*/}/${in_dir##*/}_peaks.bed
	        if [ -e $peaks_file ]
	           then
        	      # put the peak summit as start and peak summit + 1 as end 
	              awk -v OFS='\t' '{print $1,$7,$7+1,"'${in_dir##*/}'",$5,"."}' $peaks_file > $out_file
	           else
        	      echo "Folder $in_dir does not contain a results file. Check the BCP log file for more details. Skipping..";
	              exit 1
	        fi
	  elif [ $data_type = "GEM" ]
	     then
	       # echo "I'm in GEM";
	        #normally the ones from round 0 should be better, but they do not output it anymore, so we go for round 1 (read doc)
	        #peaks_file=${in_dir%/*}/${in_dir##*/}/${in_dir##*/}/${in_dir##*/}_outputs/${in_dir##*/}_1.GEM_events.bed
		peaks_file=(`find $in_dir -name "*GEM_events.bed"`)
		echo "Peak file: $peaks_file"
	        if [ -e $peaks_file ]
	           then
	              # add the peak summit offset to the start (always 100bp in the case of GEM)
	              awk -v OFS='\t' '{print $1,$2+100,$2+101,"'${in_dir##*/}'",$5,$6}' $peaks_file > $out_file
	           else
	              echo "Folder $in_dir does not contain a results file. Check the GEM log file for more details. Skipping..";
              	      exit 1
	        fi
	  else
	     echo "No supported data_type passed as argument. Only MACS, GEM, BCP, HOMER supported.";
	     exit 1
	  fi
    
     #put toghether the log file name
     log=$out_dir/${in_dir##*/}.log
     logme "Start time: $(date)"
     # keep output file "template"
     peaks_file=$out_file
     logme "Input peak file: $peaks_file"
     logme "Sha256 input file: $(sha256sum $peaks_file)"
     logme "Input file size: $(du -h $peaks_file | cut -f1)"

     # DEBUG
#    echo "Peaks file: $peaks_file"
#    echo "Out dir: $out_dir"
#    echo "Out file: $out_file"
#    echo "Log file: $log"
#    exit 0

     ##################### Check if we need to generate foreground and background training files ########################

   
     # check if models requiring training are called
     if [ $model = "DAMO" ] || [ $model = "TFFM" ] || [ $model = "DNA" ] || [ $model = "ALL" ]
         then

             logme "Model requiring trainig files called: "$model". Checking for training and test files..."
             out_dir_train=$out_dir'/training_files'
             out_file_train=$out_dir_train/$(basename $out_file)
 
             #test files - create the file name and check if generated
             foreground_test_file_bed=${out_file_train}_${window_size}'bp'
             foreground_test_file_fa=${foreground_test_file_bed}'.fa'
            
             # check if training folder exists and at least the first file generated 
             if [ ! -d $out_dir_train ] || [[ ! -f $foreground_test_file_fa ]]
                 then
                     echo "No foreground and/or background files were generated for training. They will be generated now with BiasAway..." 
                     if [ ! -d $BACKGROUND_REPOSITORY ]
                           then
                               echo "No BiasAway repository was generated. It will be generated now (this is done only once). It will take between 2 and 3 hours..."
    			       bash $GENERATE_BACKGROUND_REPOSITORY $species
                     fi
                     logme "Background files generation start time: $(date)"
                     bash $GENERATE_BACKGROUND_FILES $in_dir $data_type $out_dir $species $tf_name $window_size $training_window_size
		     logme "Background files generation end time: $(date)"
              fi

              # check if the repository was actually created
              if [ "$?" -ne 0 ]
                  then
                      echo "Failed to generatethe BiasAway repository. Exiting..."
              fi  

              # reassign all of them them 

              #test files
              foreground_test_file_bed=${out_file_train}_${window_size}'bp'
              foreground_test_file_fa=${foreground_test_file_bed}'.fa'
              
              #training files
              foreground_file_bed=${out_file_train}_${training_window_size}'bp'
              foreground_file_fa=${foreground_file_bed}'.fa'

              background_file_bed=${foreground_file_bed}'.background'
              background_file_fa=${foreground_file_fa}'.background.fa'
     fi

#   #DEBUG
#   logme "Foreground test BED: $foreground_test_file_bed"
#   logme "Foreground test FASTA: $foreground_test_file_fa"
#   logme "Foreground train BED: $foreground_file_bed"
#   logme "Foreground train FASTA: $foreground_file_fa"
#   logme "Background file BED: $background_file_bed"
#   logme "Background file FASTA: $background_file_fa"
#   logme "-----------------------------------------------------------"
  
  to_remove=()
  
  #################### PWM model ########################
  if [ $model = "PWM" ] || [ $model = "ALL" ]
      then

          logme "Applying the PWM model..."

          # retrieve the PFM(s) if not specified by user
          if [ -z $pfm ]
            then
                pfms=($(retrieve_pfms $PWM_FOLDER))
          fi
         
           mkdir -p $out_dir'/PWM'
          
          ################### 1. Increase area around the peak summit ##################
	  $PATH_TO_BEDTOOLS2/bedtools slop -i $out_file -g $CHROMOSOME_FILE -b $window_size > $out_dir'/PWM/'$(basename $out_file)_${window_size}bp
          to_remove+=($out_file)
          out_file=$out_dir'/PWM/'$(basename $out_file)
          out_file_bckp=$out_file
 
	  ############## 2. Map back to the genome and get a FASTA file #################
	  $PATH_TO_BEDTOOLS2/fastaFromBed -fo ${out_file}_${window_size}bp.fa -fi $GENOME_FILE -bed ${out_file}_${window_size}bp
          to_remove+=(${out_file}_${window_size}bp)
         
          for p in "${pfms[@]}"
             do
               if [[ ! -z $p ]]
                  then
	            logme "PWM: using the PFM: $(basename $p) for folder ${in_dir#/*} with the TF name $tf_name"
                    out_file=$out_file_bckp
               else
                  continue
               fi
  
               # retrieve the current PFM identifier to be added to the output file
               pfm_id=$(basename $p)
              ################# 3. Scoring the sequences and output ###################
	      $PWM_SCORING_CODE $p ${out_file}_${window_size}bp.fa 0.0 -b -n $tf_name > $out_dir'/PWM/'${data_type}_${tf_name}_${pfm_id}
              to_remove+=(${out_file}_${window_size}bp.fa)
	      out_file=$out_dir'/PWM/'${data_type}_${tf_name}_${pfm_id}
	      # Output format:
	      # 1. Chromosome:start-end (end-start is SLOP (window_size*2)+1
	      # 2. Description
	      # 3. TF name
	      # 4. Strand
	      # 5. Absolute score
	      # 6. Relative score
	      # 7. Start offset of the TFBS in the sequence from the window start (Note it is one based, so no need to +1)
	      # 8. End of TFBS relative to start offset (8th column - 7th column gives the TFBS sequence length)
	      # 9. Sequence (Note: that it is 1 based, so there is no need to +1 the start offset of the TFBS 

	      ################# 4. Format file for thresholding ####################
	      awk -v OFS='\t' '{print $1,$4,$9,$3,(($8-$7)+1),(($7+$8)/2)-'$window_size',$6,$5,$7,$8}' $out_file | \
              awk -v OFS='\t' -F":" '$1=$1' | \
              awk -v OFS='\t' '{sub(/\-/,"\t",$2)};1' > ${out_file}.score
              to_remove+=($out_file)
              # Output format:
	      # 1. Chromosome
	      # 2. Start of the TFBS on chromosome (chrStart from step 4 + column 7 from step 4)
	      # 3. End of the TFBS on chromosome (resulting chrStart + length of TFBS (8th column - 7th column from step 4)
	      # 4. Strand
	      # 5. Sequence
	      # 6. TF name
	      # 7. TFBS sequence length (Note that +1 because it is zero based coming out of the C code)
	      # 8. Distance to peak max (!!Note: that this is relative to the chrStart and chrEnd info coming from SLOP)
	      # 9. Relative score
	      # 10. Absolute score
	      # 11. Start offset of the TFBS relative to window start
	      # 12. End of the TFBS relative to window start

	      #################### 5. Run the thresholding script ##################
	      # Parameters:
	      #  1. Path to script wrapper for threshold calculation
	      #  2. Input file containing the scored sequences
	      #  3. Type of score (relative, prob or abs) - same as score.type parameter in compute_thresholds.R
	      #  4. The column holding the TFBS sequence length - same as seq.length.column in compute_thresholds.R
	      #  5. The column holding the distance to the peak summit - same as distance.column parameter in compute_thresholds.R
	      #  6. The column containing the score - same as score.column parameter in compute_thresholds.R
	      #  7. The size of the window used in bedtools slop
	      #  8. The name of the output file - same as output.file.name parameter in compute_threshold.R
	      thresholds=$(Rscript $CALL_THRESHOLDS_SCRIPT $COMPUTE_THRESHOLDS_SCRIPT $CHIP_EAT_FOLDER $ENTROPY_CODE ${out_file}.score "relative" 7 8 9 $window_size ${out_file}.ez)
	      if [ "$thresholds" !=  "OK" ]	
                  then
                     logme "No thresholds calculated for ${in_dir##*/}. Maybe signal is too poor :/"
                     continue
              fi
              
              #################### 6. Create BED6 file with the TFBSs #################
              if [ -f ${out_file}.ez ] && [ -s ${out_file}.ez ]
                   then
	              awk -v OFS='\t' '{print $1, ($2+$11), ($2+$12), $6 "_" $5, ".", $4}' ${out_file}.ez > ${out_file}.tfbs.bed
              fi
         done #end for over PFMs
         logme "Done with the PWM model."
    fi

    #################### BINDING ENERGY MODEL ########################
    if [ $model = "BEM" ] || [ $model = "ALL" ]
        then
           logme "Applying the binding energy model..."
           # retrieve the PFM(s) if not specified by user
           if [ -z $pfm ]
              then
                 pfms=($(retrieve_pfms $MEME_FOLDER))
           fi
           
           mkdir -p $out_dir'/BEM' 
           out_file=$peaks_file

           ################### 1. Increase area around the peak summit ##################
           $PATH_TO_BEDTOOLS2/bedtools slop -i $out_file -g $CHROMOSOME_FILE -b $window_size > $out_dir'/BEM/'$(basename $out_file)_${window_size}bp
           to_remove+=($out_file)
           out_file=$out_dir'/BEM/'$(basename $out_file)
           out_file_bckp=$out_file

           ############## 2. Map back to the genome and get a FASTA file #################
           $PATH_TO_BEDTOOLS2/fastaFromBed -tab -fo ${out_file}_${window_size}bp.fa -fi $GENOME_FILE -bed ${out_file}_${window_size}bp
           to_remove+=(${out_file}_${window_size}bp)
               
           for p in "${pfms[@]}"
             do
               if [[ ! -z $p ]]
                  then
                    logme "BEM: using the PFM: $(basename $p) for folder ${in_dir#/*} with the TF name $tf_name"
                    out_file=$out_file_bckp
               else
                  continue
               fi

               #retrieve the current PFM identifier to be added to the output file
               pfm_id=$(basename $p)

               ################# 3. Scoring the sequences and output ###################
               python2.7 $BEM_SCORING_CODE --input ${out_file}_${window_size}bp.fa \
               --pfm $p --outdir $out_dir'/BEM' --tf $tf_name \
               --outfile $out_dir'/BEM/'${data_type}_${tf_name}_${pfm_id}              
               to_remove+=(${out_file}_${window_size}bp.fa)
               out_file=$out_dir'/BEM/'${data_type}_${tf_name}_${pfm_id}
               # Output format:
	       # 1. Chromosome:start-end (end-start is SLOP (window_size*2)+1
	       # 2. Description (default BEM)
	       # 3. TF name
	       # 4. Strand
	       # 5. Energy score (between 0 and 100) - 100 the highest/best (reverted from the original implementation)
	       # 6. Start offset of the TFBS in the sequence from the window start (Note it is one based, so no need to +1)
	       # 7. End of TFBS relative to start offset (8th column - 7th column gives the TFBS sequence length)
	       # 8. Sequence (Note: that it is 1 based, so there is no need to +1 the start offset of the TFBS 

	      ################# 4. Format file for centrimo and visualization tools ####################
              awk -v OFS='\t' '{print $1,$4,$8,$3, (($7-$6)+1),(($6+$7)/2)-'$window_size',$5,$2,$6,$7}' $out_file | \
              awk -v OFS='\t' -F":" '$1=$1' | \
              awk -v OFS='\t' '{sub(/\-/,"\t",$2)};1' > ${out_file}.score
              to_remove+=($out_file)
	      # Output format:
	      # 1. Chromosome
	      # 2. Start of the TFBS on chromosome (chrStart from step 4 + column 7 from step 4)
	      #	3. End of the TFBS on chromosome (resulting chrStart + length of TFBS (8th column - 7th column from step 4)
	      # 4. Strand
	      # 5. Sequence
	      # 6. TF name
	      # 7. TFBS sequence length (Note that +1 because it is zero based coming out of the energy score code)
	      # 8. Distance to peak max (!!Note: that this is relative to the chrStart and chrEnd info coming from SLOP)
	      # 9. Energy score
	      # 10. Description
	      # 11. Start offset of the TFBS relative to window start
	      # 12. End of the TFBS relative to window start

              #################### 5. Run the thresholding script ##################
      	      #  1. Path to script wrapper for threshold calculation
	      #  2. Input file containing the scored sequences (formatted file from step 5)
	      #  3. Type of score (relative, prob or abs) - same as score.type parameter in compute_thresholds.R
	      #  4. The column holding the TFBS sequence length - same as seq.length.column in compute_thresholds.R
	      #  5. The column holding the distance to the peak summit - same as distance.column parameter in compute_thresholds.R
	      #  6. The column containing the score - same as score.column parameter in compute_thresholds.R
	      #  7. The size of the window used in bedtools slop
	      #  8. The name of the output file - same as output.file.name parameter in compute_threshold.R
	      thresholds=$(Rscript $CALL_THRESHOLDS_SCRIPT $COMPUTE_THRESHOLDS_SCRIPT $CHIP_EAT_FOLDER $ENTROPY_CODE ${out_file}.score "prob" 7 8 9 $window_size ${out_file}.ez)
              if [ "$thresholds" != "OK" ]
		then
                     logme "No thresholds calculated for ${in_dir##*/}. Maybe signal is too poor :/"
                     continue
              fi

              #################### 6. Create BED6 file with the TFBSs #################
              if [ -f ${out_file}.ez ] && [ -s ${out_file}.ez ]
                   then
                      awk -v OFS='\t' '{print $1, ($2+$11), ($2+$12), $6 "_" $5, ".", $4}' ${out_file}.ez > ${out_file}.tfbs.bed
              fi
         done #end for over PFMs
         logme "Done with the binding energy model."
     fi
     #################### DAMO MODEL ####################
     if [ $model = "DAMO" ] || [ $model = "ALL" ]
          then
             logme "Applying the DAMO model. This might take a while..."
             # retrieve the PFM(s) if not specified by user
             if [ -z $pfm ]
                then
                   pfms=($(retrieve_pfms $PFM_FOLDER))
             fi

             out_file_bckp=$out_dir/${data_type}_${tf_name}_peaks
             for p in "${pfms[@]}"
                do
                  if [[ ! -z $p ]]
                     then
                         logme "DAMO: using the PFM: $(basename $p) for folder ${in_dir#/*} with the TF name $tf_name"
                         out_file=$out_file_bckp
                  else
                     continue
                  fi

                   #retrieve the current PFM identifier to be added to the output file
	           pfm_id=$(basename $p)
                   mkdir -p $out_dir'/DAMO'
                   
                   ################# 3. Train DAMO on the foreground files ###################
                   #Rscript $DIMO_APPLY_SCODE $DIMO_TRAIN_SCODE $CHIP_EAT_FOLDER $foreground_file_fa $background_file_fa $p $out_dir'/DiMO'
		   logme "DAMO start time: $(date)"
		   bash $DAMO_TRAIN_CODE $foreground_file_fa $background_file_fa $p $out_dir'/DAMO/'
		   logme "DAMO end time: $(date)"
		   sed 's/^.: //g; s/ /\t/g' $out_dir'/DAMO/DAMO_END_PWM.txt' > $out_dir'/DAMO/DAMO_END_PWM_out.txt'
		   mv $out_dir'/DAMO/DAMO_END_PWM_out.txt' $out_dir'/DAMO/'${pfm_id}.damo.pwm

                   #note that it assumes ".damo.pwm" is added to the file containing the PWM. Modify accordingly if $train_dimo is modified
                   new_pwm=$out_dir'/DAMO/'$(basename $p).damo.pwm
                  
                   ################# 4. Score the sequences with the new PWM output by DAMO ###############
                   logme "No rescan start time: $(date)"
		   $PWM_SCORING_CODE $new_pwm $foreground_test_file_fa 0.0 -b -n $tf_name > $out_dir'/DAMO/'${data_type}_${tf_name}_${new_pwm##*/}_norescan
		   logme "No rescan end time: $(date)"
		   logme "Rescan start time: $(date)"
		   $PWM_SCORING_CODE $new_pwm $foreground_test_file_fa 0.0 -n $tf_name > $out_dir'/DAMO/'${data_type}_${tf_name}_${new_pwm##*/}_rescan
		   logme "Rescan end time: $(date)"
                   out_file=$out_dir'/DAMO/'${data_type}_${tf_name}_${new_pwm##*/}_norescan
		   out_file_rescan=$out_dir'/DAMO/'${data_type}_${tf_name}_${new_pwm##*/}_rescan
                   
                   ################# 5. Format file for thresholding ####################
		   Rscript $REVERSE_COMPLEMENT_CODE $out_file
	           #awk -v OFS='\t' '{print $1,$4,$9,$3,(($8-$7)+1),(($7+$8)/2)-'$window_size',$6,$5,$7,$8}' $out_file | \
                   #awk -v OFS='\t' -F":" '$1=$1' | \
                   #awk -v OFS='\t' '{sub(/\-/,"\t",$2)};1' > ${out_file}.score

		   awk -v OFS='\t' -F"::" '$1=$1' $out_file | \
		   awk -v OFS='\t' '{print $2,$3,$4,$5,$6,$7,$8,$9,$10,$1}' | \
		   awk -v OFS='\t' -F":" '$1=$1' | \
		   awk -v OFS='\t' '{gsub(/\-/, "\t", $2)} 1' | \
		   awk -v OFS='\t' '{print $1,$2,$3,$6,$11,$5,(($10-$9)+1),(($9+$10)/2)-'$window_size',$8,$7,$9,$10,$12}' > ${out_file}.score

		   Rscript $REVERSE_COMPLEMENT_CODE $out_file_rescan
		   #awk -v OFS='\t' '{print $1,$4,$9,$3,(($8-$7)+1),(($7+$8)/2)-'$window_size',$6,$5,$7,$8}' $out_file_rescan | \
                   #awk -v OFS='\t' -F":" '$1=$1' | \
                   #awk -v OFS='\t' '{sub(/\-/,"\t",$2)};1' > ${out_file_rescan}.score

		   awk -v OFS='\t' -F"::" '$1=$1' $out_file_rescan | \
                   awk -v OFS='\t' '{print $2,$3,$4,$5,$6,$7,$8,$9,$10,$1}' | \
                   awk -v OFS='\t' -F":" '$1=$1' | \
                   awk -v OFS='\t' '{gsub(/\-/, "\t", $2)} 1' | \
                   awk -v OFS='\t' '{print $1,$2,$3,$6,$11,$5,(($10-$9)+1),(($9+$10)/2)-'$window_size',$8,$7,$9,$10,$12}' > ${out_file_rescan}.score

                   to_remove+=($out_file)
		   to_remove+=($out_file_rescan)
		   to_remove+=(${out_file_rescan}.score)
        	   # Output format:
	           # 1. Chromosome
                   # 2. Start of the TFBS on chromosome (chrStart from step 4 + column 7 from step 4)
                   # 3. End of the TFBS on chromosome (resulting chrStart + length of TFBS (8th column - 7th column from step 4)
                   # 4. Strand
                   # 5. Sequence
                   # 6. TF name
                   # 7. TFBS sequence length (Note that +1 because it is zero based coming out of the C code)
                   # 8. Distance to peak max (!!Note: that this is relative to the chrStart and chrEnd info coming from SLOP)
                   # 9. Relative score
                   # 10. Absolute score
                   # 11. Start offset of the TFBS relative to window start
                   # 12. End of the TFBS relative to window start

                   #################### 6. Run the thresholding script ##################
                   # Parameters:
                   #  1. Path to script wrapper for threshold calculation
                   #  2. Input file containing the scored sequences (formatted file from step 5)
                   #  3. Type of score (relative, prob or abs) - same as score.type parameter in compute_thresholds.R
                   #  4. The column holding the TFBS sequence length - same as seq.length.column in compute_thresholds.R
                   #  5. The column holding the distance to the peak summit - same as distance.column parameter in compute_thresholds.R
                   #  6. The column containing the score - same as score.column parameter in compute_thresholds.R
                   #  7. The size of the window used in bedtools slop
                   #  8. The name of the output file - same as output.file.name parameter in compute_threshold.R
		   logme "Threshold finding start time: $(date)"
                   thresholds=$(Rscript $CALL_THRESHOLDS_SCRIPT $COMPUTE_THRESHOLDS_SCRIPT $CHIP_EAT_FOLDER $ENTROPY_CODE ${out_file}.score "relative" 7 8 9 $window_size ${out_file}.ez)
		   logme "Threshold finding end time: $(date)"
                   if [ "$thresholds" !=  "OK" ]
                      then
                         logme "No thresholds calculated for ${in_dir##*/}. Maybe signal is too poor :/"
                         continue
                  fi
                 
                  #################### 7. Create BED6 file with the non-rescanned TFBSs #################
                  if [ -f ${out_file}.ez ] && [ -s ${out_file}.ez ]
                      then
                          awk -v OFS='\t' '{print $1, ($2+$11), ($2+$12), $6 "_" $5, ".", $4}' ${out_file}.ez > ${out_file}.tfbs.bed
                  fi

		  ################### 8. Subset rescanned sequences within the enrichment zone and create final output file ###################
		  if [ $data_type = "MACS" ]
                    then
                      peaks_file=(`find $in_dir -name "*_peaks.narrowPeak"`)
		      Rscript $JOIN_RESCANNED_PEAKS ${out_file}.score.thr ${out_file}.score ${out_file_rescan}.score $peaks_file
                  fi
		  Rscript $GET_BEST_TFBS ${out_file_rescan}.score.ez.score

		  ################### 9. Recover original ChIP-seq peaks containing TFBSs ####################
		  if [ $data_type = "MACS" ]
		    then
		      peaks_file=(`find $in_dir -name "*_peaks.narrowPeak"`)
		      Rscript $RECOVER_MACS $peaks_file ${out_file_rescan}.score.ez.score_best.tsv ${out_file_rescan}'_peaks_with_tfbs_ez.bed'
           	  fi
	   done #end for over PFMs
           logme "Done with the DAMO model."
	   logme "Path to output file: ${out_dir}'/DAMO/peaks_with_tfbs_ez.bed'"
	   logme "Sha256 of output file: $(sha256sum ${out_dir}'/DAMO/peaks_with_tfbs_ez.bed')"
	   logme "Output file size: $(du -h ${out_dir}'/DAMO/peaks_with_tfbs_ez.bed' | cut -f1)"
   fi
   ######################### TRANSCRIPTION FACTOR FLEXIBLE MODEL #######################
   if [ $model = "TFFM" ] || [ $model = "ALL" ]
       then
          logme "Applying the TFFM model..."
          # retrieve the PFM(s) if not specified by user
          if [ -z $pfm ]
             then
                pfms=($(retrieve_pfms $PFM_FOLDER))
          fi
          
          out_file_bckp=$out_dir/${data_type}_${tf_name}_peaks
          for p in "${pfms[@]}"
             	do
               if [[ ! -z $p ]]
                  then
                    logme "TFFM: using the PFM: $(basename $p) for folder ${in_dir#/*} with the TF name $tf_name"
                    out_file=$out_file_bckp
               else
                  continue
               fi

               #retrieve the current PFM identifier to be added to the output file
               pfm_id=$(basename $p)
               mkdir -p $out_dir'/TFFM'

               ################# 5. Scoring the sequences and output ###################
               mkdir -p $out_dir'/TFFM'
               python2.7 $TFFM_SCORING_CODE_FO --train $foreground_file_fa --input $foreground_test_file_fa \
               --pfm $p --outdir $out_dir'/TFFM' --tf $tf_name \
               --outfile $out_dir'/TFFM/'${data_type}_${tf_name}_${pfm_id}
               out_file=$out_dir'/TFFM/'${data_type}_${tf_name}_${pfm_id}
              # Output format:
	      # 1. Chromosome:start-end (end-start is SLOP (window_size*2)+1
	      # 2. Start offset of the TFBS in the sequence from the window start (Note it is one based, so no need to +1)
	      # 3. End of TFBS relative to start offset (2nd column - 3rd column gives the TFBS sequence length)
	      # 4. Strand
	      # 5. Sequence (Note: that it is 1 based, so there is no need to +1 the start offset of the TFBS 
	      # 6. TF name - default TFFM
	      # 7. HMM state
	      # 8. Score - from 0 to 1 (1 is highest)

	      ################# 6. Format file for centrimo and visualization tools ####################
	      awk -v OFS='\t' '{print $1,$4,$5,$6,(($3-$2)+1),(($2+$3)/2)-'$window_size',$8,$7,$2,$3}' $out_file | \
              awk -v OFS='\t' -F":" '$1=$1' | \
              awk -v OFS='\t' '{sub(/\-/,"\t",$2)};1' > ${out_file}.score
              to_remove+=($out_file)
     	      # Output format:
	      # 1. Chromosome
	      # 2. Start of the TFBS on chromosome (chrStart from step 4 + column 7 from step 4)
	      # 3. End of the TFBS on chromosome (resulting chrStart + length of TFBS (8th column - 7th column from step 4)
	      # 4. Strand
	      # 5. Sequence
	      # 6. TF name
	      # 7. TFBS sequence length (Note that +1 because it is zero based coming out of the TFFM code)
	      # 8. Distance to peak max (!!Note: that this is relative to the chrStart and chrEnd info coming from SLOP)
	      # 9. Sequence score - based on probability coming out of TFFM
	      # 10. State of the HMM
	      # 11. Start offset of the TFBS relative to window start
	      # 12. End of the TFBS relative to window start

	      #################### 7. Run the thresholding script ##################
	      #  1. Path to script wrapper for threshold calculation
	      #  2. Input file containing the scored sequences (formatted file from step 5)
	      #  3. Type of score (relative, prob or abs) - same as score.type parameter in compute_thresholds.R
	      #  4. The column holding the TFBS sequence length - same as seq.length.column in compute_thresholds.R
	      #  5. The column holding the distance to the peak summit - same as distance.column parameter in compute_thresholds.R
	      #  6. The column containing the score - same as score.column parameter in compute_thresholds.R
	      #  7. The size of the window used in bedtools slop
	      #  8. The name of the output file - same as output.file.name parameter in compute_threshold.R
	      thresholds=$(Rscript $CALL_THRESHOLDS_SCRIPT $COMPUTE_THRESHOLDS_SCRIPT $CHIP_EAT_FOLDER $ENTROPY_CODE ${out_file}.score "prob" 7 8 9 $window_size ${out_file}.ez)
              if [ "$thresholds" !=  "OK" ]
                   then
                      logme "No thresholds calculated for ${in_dir##*/}. Maybe signal is too poor :/"
                  fi

              #################### 7. Create BED6 file with the TFBSs #################
              if [ -f ${out_file}.ez ] && [ -s ${out_file}.ez ]
                  then
                      awk -v OFS='\t' '{print $1, ($2+$11), ($2+$12), $6 "_" $5, ".", $4}' ${out_file}.ez > ${out_file}.tfbs.bed
              fi
           done #end for over PFMs
           logme "Done with the TFFM model."
  fi
  ################### DNA SHAPED TFBS MODEL ######################
  if [ $model = "DNA" ] || [ $model = "ALL" ]
       then
          logme "Applying the DNA shaped TFBS model..."
          # retrieve the PFM(s) if not specified by user
          if [ -z $pfm ]
             then
                pfms=($(retrieve_pfms $PFM_FOLDER))
          fi
          
          if [ ! -d $PATH_TO_GB_SHAPE ]
              then 
                  logme "No folder containing DNA shape features found. Check the config.sh file. Exiting...."
                  exit 1
          fi
          
          out_file_bckp=$out_dir/${data_type}_${tf_name}_peaks
          for p in "${pfms[@]}"
                do
               if [[ ! -z $p ]]
                  then
                    logme "DNAshaped: using the PFM: $(basename $p) for folder ${in_dir#/*} with the TF name $tf_name"
                    out_file=$out_file_bckp
               else
                  continue
               fi

               #retrieve the current PFM identifier to be added to the output file
               pfm_id=$(basename $p)
               mkdir -p $out_dir'/DNA'
               
               #check if the TFFM initialization files files actually exist, if not skip the folder
               tffm_xml=(`find $out_dir'/TFFM/' -name "*$pfm_id*tffm.xml"`)
               if [ ${#tffm_xml[@]} -eq 0  ]
                    then 
                       logme "The TFFM configuration file was not found. DNA+TFFM will be skipped. Run the TFFM model before the DNA shaped."
               else
                    tffm_xml=${tffm_xml[0]}
                    cp $tffm_xml $out_dir'/DNA'
                    echo "TFFM file: $tffm_xml"
               fi


          #will keep the results files to calculate thresholds for
          results=()
          out_file=$out_dir'/DNA/'${data_type}_${tf_name}_${pfm_id}

          ################# 7. Scoring the sequences and output ###################

          #check if already ran
          if  [ -f $tffm_xml ]
            then
               if [ ! -f "${out_file}_DNAshaped_tffm_fo" ] || [ ! -s "${out_file}_DNAshaped_tffm_fo" ]
                then  
                    logme "Training a first order TFFM + DNA shape classifier."
                    python2.7 $DNA_SHAPED_CODE trainTFFM -T $tffm_xml \
           	    -i $foreground_file_fa -I $foreground_file_bed \
       	            -b $background_file_fa -B $background_file_bed \
                    -o $out_dir'/DNA/'DNAshapedTFFM-fo_classifier -t first_order \
                    -1 $HELT $PROT $MGW $ROLL -2 $HELT2 $PROT2 $MGW2 $ROLL2 -n >> $log 2>&1;
               else
                   logme "Already found processed file for ${out_file}_DNAshaped_tffm_fo"
              fi
          ## commented due to high similarity of results between first order and detailed
 #        logme "Training a detailed TFFM + DNA shape classifier.";
 #        python2.7 $DNA_SHAPED_CODE trainTFFM -T $tffm_detailed_xml \
 #        -i $foreground_file_fa -I $foreground_file_bed \
 #        -b $background_file_fa -B $background_file_bed \
 #        -o $out_dir'/DNA/'DNAshapedTFFM_d_classifier -t detailed \
 #        -1 $HELT $PROT $MGW $ROLL -2 $HELT2 $PROT2 $MGW2 $ROLL2 -n >> $log 2>&1;
               
         fi

         if [ ! -f "${out_file}_DNAshaped_pssm" ] || [ ! -s "${out_file}_DNAshaped_pssm" ]
            then
                 logme "Training a PSSM + DNA shape classifier."
                 python2.7 $DNA_SHAPED_CODE trainPSSM -f $p \
                 -i $foreground_file_fa -I $foreground_file_bed \
	         -b $background_file_fa -B $background_file_bed \
        	 -o $out_dir'/DNA/'DNAshapedPSSM_classifier \
                 -1 $HELT $PROT $MGW $ROLL -2 $HELT2 $PROT2 $MGW2 $ROLL2 -n >> $log 2>&1;
            else
                logme "Already found processed file for ${out_file}_DNAshaped_pssm"
         fi
  
         if [ ! -f "${out_file}_DNAshaped_4bits" ] || [ ! -s "${out_file}_DNAshaped_4bits" ]
           then
                logme "Training a 4-bits + DNA shape classifier.";
                python2.7 $DNA_SHAPED_CODE train4bits -f $p \
                -i $foreground_file_fa -I $foreground_file_bed \
                -b $background_file_fa -B $background_file_bed \
                -o $out_dir'/DNA/'DNAshaped4bits_classifier \
                -1 $HELT $PROT $MGW $ROLL -2 $HELT2 $PROT2 $MGW2 $ROLL2 -n >> $log 2>&1;
         else
               logme "Already found processed file for ${out_file}_DNAshaped_4bits"
         fi

         if  [ -f $tffm_xml ]
           then
              if [ ! -f "${out_file}_DNAshaped_tffm_fo" ] || [ ! -s "${out_file}_DNAshaped_tffm_fo" ]
              then
                 logme "Applying the trained first order TFFM + DNA shape classifier on foreground sequences."
                 python2.7 $DNA_SHAPED_CODE applyTFFM -T $tffm_xml \
                 -i $foreground_test_file_fa -I $foreground_test_file_bed \
                 -c $out_dir'/DNA/'DNAshapedTFFM-fo_classifier.pkl -o ${out_file}_$(basename $pfm)_DNAshaped_tffm-fo \
                 -1 $HELT $PROT $MGW $ROLL -2 $HELT2 $PROT2 $MGW2 $ROLL2 -n >> $log 2>&1;
                 results+=(${out_file}_DNAshaped_tffm-fo)
          fi  
          ## commented due to high similarity of results between first order and detailed
#          logme "Applying the trained detailed TFFM + DNA shape classifier on foreground sequences.";
#          python2.7 $DNA_SHAPED_CODE applyTFFM -T $tffm_detailed_xml \
#          -i $foreground_test_file_fa -I $foreground_test_file_bed \
#          -c $out_dir'/DNA/'DNAshapedTFFM_d_classifier.pkl -o ${out_file}_DNAshaped_tffm-detailed \
#          -1 $HELT $PROT $MGW $ROLL -2 $HELT2 $PROT2 $MGW2 $ROLL2 -n >> $log 2>&1;
#          results+=(${out_file}_DNAshaped_tffm_detailed)
        fi 
      

        if [ ! -f "${out_file}_DNAshaped_pssm" ] || [ ! -s "${out_file}_DNAshaped_pssm" ]
            then
               logme "Applying the trained PSSM + DNA shape classifier on foreground sequences."
               python2.7 $DNA_SHAPED_CODE applyPSSM -f $p \
               -i $foreground_test_file_fa -I $foreground_test_file_bed \
               -c $out_dir'/DNA/'DNAshapedPSSM_classifier.pkl -o ${out_file}_$(basename $pfm)_DNAshaped_pssm \
               -1 $HELT $PROT $MGW $ROLL -2 $HELT2 $PROT2 $MGW2 $ROLL2 -n >> $log 2>&1;
               results+=(${out_file}_DNAshaped_pssm)
        fi
  
        if [ ! -f "${out_file}_DNAshaped_4bits" ] || [ ! -s "${out_file}_DNAshaped_4bits" ]
           then
               logme "Applying the trained 4-bits + DNA shape classifier on foreground sequences."
               python2.7 $DNA_SHAPED_CODE apply4bits -f $p \
               -i $foreground_test_file_fa -I $foreground_test_file_bed \
               -c $out_dir'/DNA/'DNAshaped4bits_classifier.pkl -o ${out_file}_$(basename $pfm)_DNAshaped_4bits \
               -1 $HELT $PROT $MGW $ROLL -2 $HELT2 $PROT2 $MGW2 $ROLL2 -n >> $log 2>&1;
               results+=(${out_file}_DNAshaped_4bits)
        fi  

        # Output format:
        # 1. Chromosome:start-end (end-start is SLOP (window_size*2)+1
        # 2. Start offset of the TFBS in the sequence from the window start (Note it is one based, so no need to +1)
        # 3. End of TFBS relative to start offset (2nd column - 3rd column gives the TFBS sequence length)
        # 4. Strand
        # 5. Sequence (Note: that it is 1 based, so there is no need to +1 the start offset of the TFBS 
        # 6. TF name - default TFFM
        # 7. HMM state
        # 8. Score - from 0 to 1 (1 is highest)

        ################# 8. Format file for centrimo and visualization tools ####################
        for res in "${results[@]}"
           do
             out_file=$res

	     sed '1d' $out_file > ${out_file}.tmp
             mv ${out_file}.tmp $out_file

	     #parse the file and split columsn
             awk -v OFS='\t' -v tf=$tf_name '{print $1,$4,$5,tf,(($3-$2)+1),(($2+$3)/2)-'$window_size',$6,".",$2,$3}' $out_file | \
             awk -v OFS='\t' -F":" '$1=$1' | \
             awk -v OFS='\t' '{sub(/\-/,"\t",$2)};1' > ${out_file}.score
             to_remove+=($out_file)
         # Output format:
         # 1. Chromosome
         # 2. Start of the TFBS on chromosome (chrStart from step 4 + column 7 from step 4)
         # 3. End of the TFBS on chromosome (resulting chrStart + length of TFBS (8th column - 7th column from step 4)
         # 4. Strand
         # 5. Sequence
         # 6. TF name
         # 7. TFBS sequence length (Note that +1 because it is zero based coming out of the TFFM code)
         # 8. Distance to peak max (!!Note: that this is relative to the chrStart and chrEnd info coming from SLOP)
         # 9. Sequence score - based on probability coming out of TFFM
         # 10. State of the HMM
         # 11. Start offset of the TFBS relative to window start
         # 12. End of the TFBS relative to window start

         #################### 9. Run the thresholding script ##################
         #  1. Path to script wrapper for threshold calculation
     	 #  2. Input file containing the scored sequences (formatted file from step 5)
	 #  3. Type of score (relative, prob or abs) - same as score.type parameter in compute_thresholds.R
         #  4. The column holding the TFBS sequence length - same as seq.length.column in compute_thresholds.R
         #  5. The column holding the distance to the peak summit - same as distance.column parameter in compute_thresholds.R
         #  6. The column containing the score - same as score.column parameter in compute_thresholds.R
         #  7. The size of the window used in bedtools slop
         #  8. The name of the output file - same as output.file.name parameter in compute_threshold.R
         thresholds=$(Rscript $CALL_THRESHOLDS_SCRIPT $COMPUTE_THRESHOLDS_SCRIPT $CHIP_EAT_FOLDER $ENTROPY_CODE ${out_file}.score "prob" 7 8 9 $window_size ${out_file}.ez)
      
         if [ "$thresholds" != "OK" ]
            then
               logme "No thresholds calculated for ${in_dir##*/}. Maybe signal is too poor :/"
         fi

         #################### 7. Create BED6 file with the TFBSs #################
         if [ -f ${out_file}.ez ] && [ -s ${out_file}.ez ]
             then
                awk -v OFS='\t' '{print $1, ($2+$11), ($2+$12), $6 "_" $5, ".", $4}' ${out_file}.ez > ${out_file}.tfbs.bed
         fi
       done #for over result files
     done #end for over PFMs
     logme "Done with the DNA shaped TFBS model."
  fi
  clean_up "${to_remove[@]}"
  logme "End time: $(date)"
