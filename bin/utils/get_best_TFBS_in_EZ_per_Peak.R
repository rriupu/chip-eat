## 1) Path to the file containing the rescanned peaks

# Steps are:

## 1. Argument parsing
## 2. Read the rescanned file
## 3. Create an ID column to make the filtering and analysis easier
## 4. Compute distribution of TFBS instances per peak
## 5. Get best TFBS per Peak

## 1. Argument parsing

message("Parsing arguments")
args = commandArgs(trailingOnly = T)

if (length(args) != 1) {
  message("Make sure to use the correct arguments and that you use absolute paths.")
  message("Argument 1: path to the file containing the rescanned peaks (extension -> *.damo.pwm_rescan.score.ez.score).")
}

filePath = args[1]

## 2. Read the rescanned file

message("Reading the rescanned TFBS file")
rescan = read.delim(filePath, sep = "\t", header = F, stringsAsFactors = F)
colnames(rescan) = c("chr", "start", "end", "strand", "sequence", "TF", "TFBS_length", "dist_to_peak_max", "Rel_score", "Abs_score", "start_offset", "end_offset", "peak_ID")

## 3. Create an ID column to make the filtering and analysis easier

rescan$ID = paste(rescan$chr, ":", rescan$start, "-", rescan$end, sep = "")

## 4. Compute distribution of TFBS instances per peak

suppressMessages(library(dplyr))

message("Computing distribution of TFBS instances per Peak")

rescan2 <-
  rescan %>%
  group_by(ID, start_offset, end_offset) %>%
  mutate(Best_rel_score = max(Rel_score)) %>%
  dplyr::filter(Best_rel_score == Rel_score)


message("Plotting the results")
pdf(file = paste(filePath, "_TFBS_per_peak.pdf", sep = ""))
hist(table(rescan2$ID), 50, xlab = "Number of TFBS per Peak", main = "Rescanned TFBS frequency per Peak")
plot(density(table(rescan2$ID)), xlab = "Number of TFBS per Peak", main = "Rescanned TFBS density per Peak")
dev.off()

#Tabla la tabla

#table(table(rescan2$ID))

## 5. Get best TFBS per Peak

message("Getting best TFBS per Peak")
rescan3 <-
  rescan %>%
  group_by(ID) %>%
  mutate(Best_rel_score = max(Rel_score)) %>%
  dplyr::filter(Best_rel_score == Rel_score)

rescan3 = rescan3[!duplicated(rescan3), ]

rescan4 <- 
  rescan3 %>%
  group_by(ID) %>%
  mutate(Best_dist = min(start_offset)) %>%
  dplyr::filter(Best_dist == start_offset)

message("Exporting table with best TFBS per Peak")
write.table(rescan4[, 1:13], file = paste(filePath, "_best.tsv", sep = ""), sep = "\t", col.names = T, row.names = F, quote = F)

message("Done! :D")
