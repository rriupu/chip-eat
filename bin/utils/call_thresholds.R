
#called by the peak_processing.sh script

args = commandArgs(trailingOnly = T)
if (length(args) != 0){
  source(args[1])
  
  #------------------------------------------------------------------------
  #     Arguments: 
  # 1. Path to the threshold calculation script (compute_thresholds.R)
  # 2. Path to the ChIP-eat pipeline
  # 3. Input data file - containing the extended score file (.score.ext resulting from the result_processing.sh script)
  # 4. Type of score to be taken into consideration (relative or prob)
  # 5. The column holding the length of the sequence
  # 6. The column holding the distance to the peak max
  # 7. The column holding the score 
  # 8. The size of the window used in the scoring
  # 9. The name of the output file
  #-----------------------------------------------------------------------

    #debug print
#    print("The threshold calculation script:")
#    print(args[1])
#    print("The ChIP-eat folder")
#    print(args[2])
#    print("The Entropy Java class path")
#    print(args[3])
#    print("Data file:")
#    print(args[4])
#    print("Score type:")
#    print(args[5])
#    print("Sequence length column:")
#    print(args[6]) 
#    print("Distance column:")
#    print(args[7])
#    print("Score column:")
#    print(args[8])
#    print("Window size:")
#    print(args[9])
#    print("Output file name:")
#    print(args[10])
 

    # load required libraries
    suppressMessages(require("rJava" , lib.loc=c(.libPaths(), file.path(args[2], "src/"))))
    library(MASS) #for the heat map (should be installed by default)

    #call the threshold calculation script
    compute_thresholds(entropy.class = args[3], data.file = args[4], score.type = args[5], seq.length.column = args[6], distance.column = args[7], score.column = args[8], window.size = args[9], output.file.name = args[10])

}  
