source "../config.sh"

pos_file=$1;
neg_file=$2;
seed=$3;
out_dir=$4;

echo "Reformatting foreground files"

#sed '/^[NATCGnatcg].*/ y/natcg/NATCG/; /N/d' $pos_file > ${pos_file}_DAMO.fa
# Convert all nucleotides to upper case
tac $pos_file | sed '/^[NATCGnatcg]/ y/natcg/NATCG/' | tac > ${pos_file}.tmp
# Convert all instances of 'N' nucleotides to 'Z' for deleting afterwards
tac ${pos_file}.tmp | sed '/^[NATCG]/ y/N/Z/' | tac > ${pos_file}.tmp2
# Remove all fasta sequences containing an 'N'
tac ${pos_file}.tmp2 | sed '/Z/,+1 d' | tac > ${pos_file}_DAMO.fa

# Clean up tmp files
rm ${pos_file}.tmp ${pos_file}.tmp2

echo "Reformatting background files"

sed '/^[AaTtCcGg]/N; s/\n//;/^$/d; /^[ATCGatcg].*/ y/atcg/ATCG/' $neg_file > ${neg_file}_DAMO.fa

echo "Adding pseudocounts to matrix and formatting for DAMO"

sed 's/ 0 / 1 /g; s/^0 /1 /g' $seed > ${out_dir}$(basename $seed).DAMO
Rscript $FORMAT_PFM_FOR_DAMO_CODE ${out_dir}$(basename $seed).DAMO $out_dir

echo "Applying DAMO"

echo ${pos_file}_DAMO.fa
echo ${neg_file}_DAMO.fa
echo ${out_dir}$(basename $seed).DAMO
echo $out_dir

python2.7 $DAMO_CODE --positive ${pos_file}_DAMO.fa --negative ${neg_file}_DAMO.fa --seed ${out_dir}$(basename $seed).DAMO --output $out_dir

