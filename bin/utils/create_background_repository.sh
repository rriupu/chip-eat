# This script is used to generate the BiasAway repository for generating
# background FASTA files matching the GC composition of the foreground files.
# The background files are used by the TFFM, DiMO and DNAshaped prediction models.
#
# It will first split the whole genome into 101 bp long sequences and then generate
# the BiasAway away repository which will be used to create individual background
# files for each dataset that is input.
#
# mask the non mappable regions of the genome based on blacklisted regions from Kundaje's lab
# http://mitra.stanford.edu/kundaje/akundaje/release/blacklists/hg38-human/
#
# usage: bash create_background_repository.sh Hsapiens

species=;

if [ -z "$1" ]
  then
        echo "CREATE_BACKGROUND_REPOSITORY: No species specified. Exiting..."
        exit 1
  else
        species=$1
fi

source "../config.sh"

#create the folder structure
if [ ! -d $BACKGROUND_MAPPABLE_GENOME ]
  then
        echo "The genome file will be prepared to create the repository for training sets"
	mkdir -p $BACKGROUND_MAPPABLE_GENOME
        #echo "Masking genome file..."
	# mask the unmappable regions of the genome
	#$PATH_TO_BEDTOOLS2/bedtools maskfasta -fi $GENOME_FILE -bed $BACKGROUND_UTILS/hg38.blacklist.bed -fo $BACKGROUND_MAPPABLE_GENOME/hg38_mappable_genome.fa
        echo "Splitting the genome file in 101bp sequences..." 
	# split the enetire genome in 101 BP sequences with FASTA headers
	$BACKGROUND_UTILS/split_genome_file < $GENOME_FILE > $BACKGROUND_MAPPABLE_GENOME/mappable_genome_split.fa
	tac $BACKGROUND_MAPPABLE_GENOME/mappable_genome_split.fa | sed -r '/^[AaTtCcGgNn].{,99}$/{N;d;}' | tac > $BACKGROUND_MAPPABLE_GENOME/mappable_genome_split.fa.tmp
	mv $BACKGROUND_MAPPABLE_GENOME/mappable_genome_split.fa.tmp $BACKGROUND_MAPPABLE_GENOME/mappable_genome_split.fa
        echo "Filtering out sequences containing NNNN..."
	# convert all sequences to upper case and remove the sequences containing NNNNNNNNNN
#	Rscript $BACKGROUND_CALL_SEQUENCE_FILTERING $BACKGROUND_SEQUENCE_FILTERING $BACKGROUND_MAPPABLE_GENOME/hg38_mappable_genome_split.fa $BACKGROUND_MAPPABLE_GENOME/hg38_mappable_genome_split_clean.fa
        tac $BACKGROUND_MAPPABLE_GENOME/mappable_genome_split.fa | sed '/^[ATCGNatcgn].*/ y/natcg/NATCG/; /N/,+1 d' | tac > $BACKGROUND_MAPPABLE_GENOME/mappable_genome_split_clean.fa
fi

# check if the genome was actually split
if [ -f $BACKGROUND_MAPPABLE_GENOME/mappable_genome_split_clean.fa ]
    then        
        echo "Generating BiasAway repository..."
	# generate repository with BiasAway
	python2.7 $GENERATE_BACKGROUND_SCRIPT g -b $BACKGROUND_MAPPABLE_GENOME/mappable_genome_split_clean.fa -r $BACKGROUND_REPOSITORY -f $BACKGROUND_DUMMY_FILE
        exit 0
else
    # or return error
    exit 1
fi 
