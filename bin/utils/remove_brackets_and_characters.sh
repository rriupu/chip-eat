#EXAMPLE

#[mariughe@biotin2 pcm_manipulation]$ sed 's/A//g' NEW_PFM_JASPAR_2018_no_brackets > NEW_PFM_JASPAR_2018_no_brackets_no_a
#[mariughe@biotin2 pcm_manipulation]$ sed 's/C//g' NEW_PFM_JASPAR_2018_no_brackets_no_a > NEW_PFM_JASPAR_2018_no_brackets
#[mariughe@biotin2 pcm_manipulation]$ sed 's/G//g' NEW_PFM_JASPAR_2018_no_brackets > NEW_PFM_JASPAR_2018_no_brackets_no_g
#[mariughe@biotin2 pcm_manipulation]$ sed 's/T//g' NEW_PFM_JASPAR_2018_no_brackets_no_g > NEW_PFM_JASPAR_2018_no_brackets
#[mariughe@biotin2 pcm_manipulation]$ sed 's/\[//g;s/\]//g' NEW_PFM_JASPAR_2018 > NEW_PFM_JASPAR_2018_no_brackets

#prepare the PFMs for the jaspar2meme conversion. Remove brackets and the ACGT characters

#usage: bash remove_brackets_and_characters.sh /storage/mathelierarea/processed/marius/ENCODE/bin/pcm_manipulation/PFM_NEW_JASPAR_PROFILES_2018/ /storage/mathelierarea/processed/marius/ENCODE/bin/pcm_manipulation/PFM_NEW_JASPAR_2018_only_digits/

#prepare the PFMs for the jaspar2meme conversion. Remove ONLY brackets and the ACGT characters



in_dir=$1
out_dir=$2

cd $in_dir
echo $in_dir

#PREPARE FOR THE TFFM PROCESSING
for pfm in $(find ./ -name "MA*")
  do
    echo $pfm
    #remove first line with PFM ID and TF name
    sed '1d' $pfm > $out_dir/$pfm.tmp 
    #remove opening and closing brackets
    sed 's/\[//g;s/\]//g' $out_dir/$pfm.tmp > $out_dir/$pfm
    #remove the A,C,G,T
    sed 's/A//g' $out_dir/$pfm > $out_dir/$pfm.tmp
    sed 's/C//g' $out_dir/$pfm.tmp > $out_dir/$pfm 
    sed 's/G//g' $out_dir/$pfm > $out_dir/$pfm.tmp
    sed 's/T//g' $out_dir/$pfm.tmp > $out_dir/$pfm
    #trim the leading white spaces (and/or tabs)
    sed -e 's/^[ \t]*//' $out_dir/$pfm > $out_dir/$pfm.tmp
    
    mv $out_dir/$pfm.tmp $out_dir/$pfm

done


#PREPARE FOR MEME (jaspar2meme
#for pfm in $(find ./ -name "*.pfm")
#  do
#
#    #remove opening and closing brackets
#    sed 's/\[//g;s/\]//g' $pfm > $out_dir/$pfm
#    #remove the A,C,G,T
#    sed 's/A//g' $out_dir/$pfm > $out_dir/$pfm.tmp
#    sed 's/C//g' $out_dir/$pfm.tmp > $out_dir/$pfm
#    sed 's/G//g' $out_dir/$pfm > $out_dir/$pfm.tmp
#    sed 's/T//g' $out_dir/$pfm.tmp > $out_dir/$pfm

#    mv $out_dir/$pfm.tmp $out_dir/$pfm
#    rm $out_dir/*.tmp

#done


