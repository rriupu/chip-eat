process = function(foreground_file = NULL, background_file = NULL, pfm_file = NULL, output_folder = NULL){

  #DEBUG
  #pfm_file = "MA0138.2.pfm"
  #foreground_file = "MACS_REST_peaks_50bp.fa"
  #background_file = "MACS_REST_peaks_50bp.fa.background.fa"
  
  #convert PCM into PFM and put it in the fancy format princess DiMO wants it
  pfm = read.table(pfm_file)
  pfm = scale(pfm,center = F, colSums(pfm))
  #colSums(pfm)
  
  #format the PFM  
  res = c()
  res[1] = paste(">", pfm_file, sep = " ")
  res[2] = paste("A","|", paste(pfm[1,], collapse = " "), sep =" ")
  res[3] = paste("C","|", paste(pfm[2,], collapse = " "), sep =" ")
  res[4] = paste("G","|", paste(pfm[3,], collapse = " "), sep =" ")
  res[5] = paste("T","|", paste(pfm[4,], collapse = " "), sep =" ")
  pfm_file = unlist(strsplit(pfm_file, split = "/"))
  pfm_file_name = pfm_file[length(pfm_file)]
  pfm_file = paste(paste(output_folder, pfm_file_name, sep = "/"), "dimo", sep = ".")
  write.table(res, pfm_file, sep = "\n", row.names = F, col.names = F, quote = F)
  
  #convert all input foreground sequences in upper case.. because princess DiMO chokes on it if not
  foreground = as.matrix(read.table(foreground_file, strip.white = T))
  res = c()
  j = 1
  for (i in 1:nrow(foreground)){
    if (!startsWith(as.character(foreground[i,1]), ">")){
      foreground[i,1] = toupper(foreground[i,1])
      #double check for other letters than A,C,G,T
      if (grepl('^[ACGT]+$', foreground[i,1])) {
        res[j] = foreground[i,1]
        j = j + 1
      }else{
        #go back and ignore this sequence and its description
        j = j - 1
      }
    }else{
      # keep the description
      res[j] = foreground[i,1]
      j = j + 1
    }
  }
  foreground_file = unlist(strsplit(foreground_file, split = "/"))
  foreground_file = paste(paste(output_folder, foreground_file[length(foreground_file)], sep = "/"), pfm_file_name, "dimo", sep = ".")
  write.table(res, foreground_file, sep = "\t", row.names = F, col.names = F, quote = F)
  
  #convert all input background sequences in upper case.. because princess DiMO chokes on it if not
  background = as.matrix(read.table(background_file, strip.white = T))
  res = c()
  j = 1
  for (i in 1:nrow(background)){
    if (!startsWith(as.character(background[i,1]), ">")){
      background[i,1] = toupper(background[i,1])
      #double check for other letters than A,C,G,T
      if (grepl('^[ACGT]+$', background[i,1])) {
        res[j] = background[i,1]
        j = j + 1
      }else{
        #go back and ignore this sequence and its description
        j = j - 1
      }
    }else{
      # keep the description
      res[j] = background[i,1]
      j = j + 1
    }
  }
  background_file = unlist(strsplit(background_file, split = "/"))
  background_file = paste(paste(output_folder, background_file[length(background_file)], sep = "/"), pfm_file_name, "dimo", sep = ".")
  write.table(res, background_file, sep = "\t", row.names = F, col.names = F, quote = F)
  
  #train DiMO
  DiMO(
    positive.file=foreground_file,
    negative.file=background_file,
    pfm.file.name=pfm_file,
    output.flag=foreground_file,
    epochs=150,
    add.at.five=0,
    add.at.three=0,
    learning.rates=seq(1,0.1,length.out=3)
  )
  
  #convert the new PFM to a PWM
  pfm = read.table(paste(foreground_file, "END.pfm", sep = "_"), fill = T)
  #get rid of the first row and the A | and C | and G | and T |
  pfm = pfm[2:nrow(pfm),3:ncol(pfm)]
  pwm = log2(pfm/0.25)  

  #note that the name assigned to the file here has direct impact on the peak_processing_DiMO_one.sh script for the $new_pwm value
  write.table(pwm, paste(pfm_file, "pwm", sep = "."), sep = "\t", row.names = F, col.names = F, quote = F)
  
  #give back the name of the generated PWM
  ### Either suppress the textual output of DiMO or just let it hardcored in the peak_processing_DiMO_one.sh scrip hardcoded to loof for a .pwm
#  cat(paste(pfm_file, "pwm", sep = "."))

}
