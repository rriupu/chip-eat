# This is a function to only trim motif 
# This is just for my purpose and included as I already have extended motif and wanted to trim it up

onlyTrim <- function(
                positive.file,
                negative.file,
                pfm.file.name,
                output.flag="check_output",
                in.seed=100
               )
{

# source("readFasta.R") ;source("checkSanity.R")  ;source("seqToSplitted.R")  ;source("readProcessSeq.R")  ;source("getComplementary.R")  ;source("splitAndGenerateList.R") ;source("indexForPiBounds.R") ;source("readCheckPFM.R") ;source("computePiBounds.R") ;source("scorePerceptron.R") ;source("eneToPFM.R") ;source("summaryOutput.R") ; source("writeFasta.R") ; source("DiMO.R") ; source("computeJustAUC.R") ; source("GenerateTrainingAndValidation.R") ; source("trimPFMafterExtension.R")

 # generate training and validation set
 GenerateTrainingAndValidation(positive.file=positive.file,in.seed=in.seed)

 if(ncol(readPFM(pfm.file.name)) <  19) {
   write("The input PFM doesnot look like extended PFM",file="")
   write("This program is to trim motif after extension (length > 19)",file="")
   quit()
 }
 else{
   trimmed.back.pfm <- trimPFMafterExtension(
                           positive.file=".validation_78619385475.fasta",
                           negative.file=negative.file,
                           pfm.file.name=pfm.file.name)
 }

 # output trimmed pfm and AUC

 opfm <- paste("trimmed_",output.flag,".pfm",sep="")

 back.pfm <- trimmed.back.pfm[[1]]
 back.auc <- trimmed.back.pfm[[2]]

 row.names(back.pfm) <- paste(row.names(back.pfm)," | ",sep="")

 write(paste(">motif1 auc on validation set: ", back.auc),file=opfm)
 write.table(back.pfm,file=opfm,col.names=FALSE,quote=FALSE,append=TRUE)

}
