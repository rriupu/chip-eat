# This is a function to extend motifs
# The input database is first split in training and validation sets
# The motifs are trained on training set and AUC is compared with the one on validation set
# If there is large difference in AUC then tell the user that over-fitting occured

trimPFMafterExtension <- function(
                positive.file,
                negative.file,
                pfm.file.name
               )
{

  penaltyPerPosition <- 0.003

  shortestMotif <- 6

  temp.in <- readPFM(pfm.file.name)
  checkPFM(temp.in)

# the max.trim.on.each.end is selected such that there are atleast four positions in the start PFM
  max.trim.on.each.end <- floor((ncol(temp.in) - shortestMotif)/2)

# keep track of AUC in this variable
  store.all.aucs.on.validation <- NULL

# start with trimmed PFM
  trimmed.pfm <-  temp.in[,c((max.trim.on.each.end+1):(ncol(temp.in)-max.trim.on.each.end))]

  write(">motif 1",file=".temp.pfm.123456789")
  write.table(file=".temp.pfm.123456789",
              trimmed.pfm,
              row.names=paste(row.names(trimmed.pfm)," | ", sep=""),
              col.names=FALSE,quote=FALSE,append=TRUE)

  temp.auc <- computeJustAUC(positive.file=positive.file,
                 negative.file=negative.file,
                 pfm.file=".temp.pfm.123456789")

  store.all.aucs.on.validation <- c(store.all.aucs.on.validation,temp.auc)

# start trimming at 5' end and trim upto max.trim
  for(i in 1:max.trim.on.each.end){

        attempted.pfm <- cbind(temp.in[max.trim.on.each.end + 1 - i],trimmed.pfm)

        write(">motif 1",file=".temp.pfm.123456789")

        write.table(file=".temp.pfm.123456789",
                     attempted.pfm,
                     row.names=paste(row.names(trimmed.pfm)," | ", sep=""),
                     col.names=FALSE,quote=FALSE,append=TRUE)

        temp.auc <- computeJustAUC(positive.file=positive.file,
                       negative.file=negative.file,
                       pfm.file=".temp.pfm.123456789")

        if( (temp.auc - penaltyPerPosition) > tail(store.all.aucs.on.validation,1) ){ print("Move accepted") }
        else{ print("Move rejected") ; break() }

        store.all.aucs.on.validation <- c(store.all.aucs.on.validation,temp.auc)

        trimmed.pfm <- attempted.pfm

  }

# start trimming at 3' position
  for(i in 1:max.trim.on.each.end){

        attempted.pfm <- cbind(trimmed.pfm, temp.in[,ncol(temp.in) - (max.trim.on.each.end - i)])

        write(">motif 1",file=".temp.pfm.123456789")

        write.table(file=".temp.pfm.123456789",
                     attempted.pfm,
                     row.names=paste(row.names(trimmed.pfm)," | ", sep=""),
                     col.names=FALSE,quote=FALSE,append=TRUE)

        temp.auc <- computeJustAUC(positive.file=".validation_78619385475.fasta",
                       negative.file=negative.file,
                       pfm.file=".temp.pfm.123456789")

        if( (temp.auc - penaltyPerPosition) > tail(store.all.aucs.on.validation,1) ){ print("Move accepted") }
        else{ print("Move rejected") ; break() }

        store.all.aucs.on.validation <- c(store.all.aucs.on.validation,temp.auc)

        trimmed.pfm <- attempted.pfm
  }

  colnames(trimmed.pfm) <- 1:ncol(trimmed.pfm)

  system("rm .validation_78619385475.fasta .temp.pfm.123456789 .training_78619385475.fasta")

  write("Done Trimming",file="")

  return(list(trimmed.pfm,tail(store.all.aucs.on.validation,1)))

}
