# This is a function to extend motifs
# The input database is first split in training and validation sets
# The motifs are trained on training set and AUC is compared with the one on validation set
# If there is large difference in AUC then tell the user that over-fitting occured

DiMOExtend <- function(
                positive.file,
                negative.file,
                pfm.file.name,
                output.flag="check_output",
                epochs=150,
                max.add=6,
                learning.rates=seq(1,0.1,length.out=3),
                in.seed=100
               )
{

 # source("readFasta.R") ;source("checkSanity.R")  ;source("seqToSplitted.R")  ;source("readProcessSeq.R")  ;source("getComplementary.R")  ;source("splitAndGenerateList.R") ;source("indexForPiBounds.R") ;source("readCheckPFM.R") ;source("computePiBounds.R") ;source("scorePerceptron.R") ;source("eneToPFM.R") ;source("summaryOutput.R") ; source("writeFasta.R") ; source("DiMO.R") ; source("computeJustAUC.R")

# generate validation set
  write("Generating training and validation sets",file="")
  pos.sam <- readFasta(positive.file)
 
  set.seed(in.seed)
  training.set <- sample(1:length(pos.sam$sequence),length(pos.sam$sequence)*0.8)
  validation.set <- setdiff(1:length(pos.sam$sequence),training.set)

  training.seq  <- pos.sam$sequence[training.set]
  training.desc <- pos.sam$description[training.set]

  validation.seq  <- pos.sam$sequence[validation.set]
  validation.desc <- pos.sam$description[validation.set]

  writeFasta(sequences=training.seq,names.sequences=training.desc,output.file=".training_78619385475.fasta")
  writeFasta(sequences=validation.seq,names.sequences=validation.desc,output.file=".validation_78619385475.fasta")

# call DiMO and get back AUC
 master.training.auc <- NULL
 master.validation.auc <- NULL
 for(i in max.add){
   training.auc <- DiMO(
                      positive.file=".training_78619385475.fasta",
                      negative.file=negative.file,
                      pfm.file.name=pfm.file.name,
                      output.flag=paste(output.flag,"_",i,sep=""),
                      epochs=epochs,
                      add.at.five=i,
                      add.at.three=i,
                      learning.rates=learning.rates
                   )

   master.training.auc <- c(master.training.auc,training.auc)

   validation.auc <- computeJustAUC(
                      positive.file=".validation_78619385475.fasta",
                      negative.file=negative.file,
                      pfm.file.name=paste(output.flag,"_",i,"_END.pfm",sep=""),
                      output.flag="temp.dimo.extend"
                   )

   master.validation.auc <- c(master.validation.auc,validation.auc)

 }

 write.table(cbind(master.training.auc,master.validation.auc),
             file=paste(output.flag,"_valid_summary.csv",sep=""),
             row.names=max.add,quote=FALSE)

}
