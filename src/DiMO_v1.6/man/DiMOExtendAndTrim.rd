\name{DiMOExtendAndTrim}
\alias{DiMOExtendAndTrim}
\title{Edits motifs first by extenstion and then by trimming}
\description{
  This is the key program to optimize motif width. First this programs extend the motif by final width of around 20 and runs the perceptron optimization. After that, the output motif is shortned to length of four. The AUC is computed on validation set. Then adds back the position at 5' end and computes AUC. If there is a significant gain in AUC then the programs accept the extension, else move to 3' end. At 3' end one by one positions are added back and if significant gain in AUC is received then accepts the extenstion else the trimming is terminated.  
}
\usage{
DiMOExtendAndTrim(
                positive.file="positive.fasta",
                negative.file="negative.fasta",
                pfm.file.name="pfm.to.be.length.otpimized.pfm",
                output.flag="check_output",
                epochs=150,
                learning.rates=seq(1,0.1,length.out=3),
                in.seed=100)

}
\arguments{
  \item{positive.file}{File name containing sequences with motifs in FASTA format}
  \item{negative.file}{File name containing sequences with motifs in FASTA format}
  \item{pfm.file.name}{PFM file with preliminary motif
     The format goes here:  
        first line with name of TF starting with ">" 
        remaining four line should be PFM  
     
     > motif 1 \cr
     A | 0.000010 0.000010 0.661491 0.000010 0.000010  \cr
     C | 0.000010 0.000010 0.338509 0.000010 0.000010  \cr
     G | 0.999700 0.999700 0.000010 0.000010 0.000010  \cr
     T | 0.000010 0.000010 0.000010 0.999700 0.999700  \cr

     This motif will be exted sequencially.
  }
  \item{output.flag}{This flag is used to generate output file names (Default: check_output).  
                     The length optimized motif will be in output file "trimmed_`output.flag`.pfm"
              }

  \item{epochs}{Number of steps of optimization. (Default: 150). Higher the number of steps, higher the time program requires to run before convergence}
  \item{learning.rate}{Learning rates used during each epoch for generating next PWM. default is c(1,0.55,0.1).}
  \item{in.seed}{Seed that will be used to randomly split training and test set. Default is 100}
}
\seealso{
  \code{\link{DiMO}} 
  \code{\link{computeJustAUC}} 
}
\examples{
% Optimize motif width by first extending seed PFM and followed by trimming. 
% The best motif is an output in file "trimmed_output_TF.pfm"

DiMOExtendAndTrim(
    positive.file="TF_positive.fasta",
    negative.file="TF_negative.fasta",
    pfm.file.name="TF.pfm",
    output.flag="output_TF",
    epochs=300,
    learning.rates=seq(1,0.1,length.out=3),
    in.seed=100
    )
}
\keyword{file}
