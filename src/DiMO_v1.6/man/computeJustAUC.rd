\name{computeJustAUC}
\alias{computeJustAUC}
\title{Program to compute AUC}
\description{
  computes AUC given a set of positive sequences, negative sequences and PFM
  Note that PFM is adjusted if there is zero and column sums are not equal to 1
}
\usage{
computeJustAUC(positive.file,negative.file,pfm.file)

}
\arguments{
  \item{positive.file}{File name containing sequences with motifs in FASTA format}
  \item{negative.file}{File name containing sequences with motifs in FASTA format}
  \item{pfm.file.name}{PFM file with preliminary motif
     The format goes here:  
        first line with name of TF starting with ">" 
        remaining four line should be PFM  
     
     > motif 1 \cr
     A | 0.000010 0.000010 0.661491 0.000010 0.000010  \cr
     C | 0.000010 0.000010 0.338509 0.000010 0.000010  \cr
     G | 0.999700 0.999700 0.000010 0.000010 0.000010  \cr
     T | 0.000010 0.000010 0.000010 0.999700 0.999700  \cr
  }
  \item{output.flag}{Output flag that will be used to output TPR/FPR}
}
\value{
  \item{auc}{Returns AUC}
}
\seealso{
  \code{\link{DiMO}}
  \code{\link{DiMOExtend}}
}
\examples{

computeJustAUC(
               positive.file="bcd_1_positive.fasta",
               negative.file="bcd_1_negative.fasta",
               pfm.file.name="bcd_1.pfm",
               output.flag="check_output"
              )

}
\keyword{file}
